import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SequenceHybridizerTest {
    private final SequenceHybridizer hybridizer = new SequenceHybridizer();

    @Test
    public void testGraphLoading() {
        final String word = "ACACGCAACTTAAA";
        final int k = 3;

        final List<String> cut = Generator.cutWord(word, 3);
        final Map<String, Node> nodes = hybridizer.createGraph(cut, k);

        final Map<String, Node> expected = createExpectedGraph();

        assertThat(nodes.entrySet(), equalTo(expected.entrySet()));
    }

    @Test
    public void testFindStartNode() {
        {
            final Map<String, Node> nodes = createExpectedGraph();
            Optional<Node> startNode = hybridizer.findStartNode(nodes);

            assertThat(startNode.isPresent(), is(true));
            assertThat(startNode.get().symbol, is("AC"));
        }
        {
            final Map<String, Node> nodes = new HashMap<>();
            nodes.put("XX", new Node("XX"));

            Optional<Node> startNode = hybridizer.findStartNode(nodes);
            assertThat(startNode.get().symbol, is("XX"));
        }
        {
            final Map<String, Node> nodes = new HashMap<>();
            nodes.put("XX", new Node("XX"));
            nodes.put("YY", new Node("YY"));
            //edges:
            nodes.get("XX").siblings.add(nodes.get("YY"));

            Optional<Node> startNode = hybridizer.findStartNode(nodes);
            assertThat(startNode.get().symbol, is("XX"));
        }
        {
            final Map<String, Node> nodes = new HashMap<>();
            nodes.put("XX", new Node("XX"));
            nodes.put("YY", new Node("YY"));
            nodes.put("ZZ", new Node("ZZ"));
            //edges:
            nodes.get("XX").siblings.add(nodes.get("YY"));
            nodes.get("YY").siblings.add(nodes.get("ZZ"));
            nodes.get("ZZ").siblings.add(nodes.get("XX"));

            Optional<Node> startNode = hybridizer.findStartNode(nodes);
            assertThat(startNode.isPresent(), is(false));

            nodes.get("YY").siblings.add(nodes.get("XX"));
            startNode = hybridizer.findStartNode(nodes);
            assertThat(startNode.get().symbol, is("YY"));
        }
    }

    @Test
    public void testInvalidGraph() {
        {
            final Map<String, Node> nodes = createExpectedGraph();
            nodes.put("XX", new Node("XX"));

            assertExceptionThrown(nodes);
        }
        {
            final Map<String, Node> nodes = new HashMap<>();
            nodes.put("XX", new Node("XX"));
            nodes.put("YY", new Node("YY"));

            assertExceptionThrown(nodes);
        }
        {
            final Map<String, Node> nodes = new HashMap<>();
            nodes.put("XX", new Node("XX"));
            nodes.put("YY", new Node("YY"));
            nodes.put("ZZ", new Node("ZZ"));
            //edges:
            nodes.get("XX").siblings.add(nodes.get("YY"));
            nodes.get("XX").siblings.add(nodes.get("ZZ"));

            assertExceptionThrown(nodes);
        }
        {
            final Map<String, Node> nodes = new HashMap<>();
            nodes.put("XX", new Node("XX"));
            nodes.put("YY", new Node("YY"));
            nodes.put("ZZ", new Node("ZZ"));
            //edges:
            nodes.get("YY").siblings.add(nodes.get("XX"));
            nodes.get("ZZ").siblings.add(nodes.get("XX"));

            assertExceptionThrown(nodes);
        }
    }

    private void assertExceptionThrown(final Map<String, Node> nodes) {
        boolean exceptionThrown = false;
        try {
            hybridizer.findStartNode(nodes);
        } catch (IllegalStateException ignored) {
            exceptionThrown = true;
        }
        assertThat(exceptionThrown, is(true));
    }

    private Map<String, Node> createExpectedGraph() {
        final Map<String, Node> expected  = new HashMap<>();

        Consumer<String> addNode = s -> expected.put(s, new Node(s));
        addNode.accept("AA");
        addNode.accept("AC");
        addNode.accept("CA");
        addNode.accept("TA");
        addNode.accept("TT");
        addNode.accept("CT");
        addNode.accept("CG");
        addNode.accept("GC");
        addNode.accept("GC");

        BiConsumer<String, String> createEdge = (src, dst) -> expected.get(src).addSibling(expected.get(dst));
        // AA
        createEdge.accept("AA", "AA");
        createEdge.accept("AA", "AC");
        // AC
        createEdge.accept("AC", "CA");
        createEdge.accept("AC", "CG");
        createEdge.accept("AC", "CT");
        // CA
        createEdge.accept("CA", "AA");
        createEdge.accept("CA", "AC");
        // TA
        createEdge.accept("TA", "AA");
        // TT
        createEdge.accept("TT", "TA");
        // CT
        createEdge.accept("CT", "TT");
        //CG
        createEdge.accept("CG", "GC");
        // GC
        createEdge.accept("GC", "CA");

        return expected;
    }




}
