import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class GeneratorTest {

    @Test
    public void testCutWord() {
        List<String> expected = Arrays.asList(
            "AAA", "AAC", "ACA", "CAC", "CAA", "ACG", "CGC", "GCA", "ACT", "CTT", "TTA", "TAA"
        );

        List<String> cut = Generator.cutWord("ACACGCAACTTAAA", 3);
        assertThat(cut, containsInAnyOrder(expected.toArray()));
    }

}
