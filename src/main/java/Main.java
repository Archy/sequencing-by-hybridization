import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) throws IOException {
        final List<String> subwords = loadSubwords();
        final SequenceHybridizer hybridizer = new SequenceHybridizer();

        Optional<String> reconstructed = hybridizer.reconstructWord(subwords, subwords.get(0).length());
        if (reconstructed.isPresent()) {
            System.out.println(reconstructed.get());
        } else {
            System.out.println("Sequence couldn't be reconstructed");
        }
    }

    private static List<String> loadSubwords() throws IOException {
        List<String> subwords = new ArrayList<>();
        int length = Integer.MIN_VALUE;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("words.txt")))) {
            String subword;
            while ((subword = reader.readLine()) != null) {
                if (subword.isEmpty()) {
                    continue;
                }
                if (length == Integer.MIN_VALUE) {
                    length = subword.length();
                } else if (length != subword.length()) {
                    throw new IllegalArgumentException("Subwords don't have same length");
                }
                subwords.add(subword);
            }
        }
        return subwords;
    }
}
