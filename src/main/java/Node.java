import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Node {
    String symbol;
    Set<Node> siblings = new HashSet<>();

    public Node(String symbol) {
        this.symbol = symbol;
    }

    public void addSibling(final Node sibling) {
        siblings.add(sibling);
    }

    public void removeSibling(String nodeSymbol) {
        siblings.removeIf(n -> n.symbol.equals(nodeSymbol));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(symbol, node.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol);
    }

    @Override
    public String toString() {
        return "Node{" +
                "symbol='" + symbol + '\'' +
                ", siblings=" + siblings.stream().map(s -> s.symbol).reduce("", (s1, s2) -> s1 + s2 + ", ") +
                '}';
    }

    /**
     * mapping needed to avoid StackOverflowError
     */
    private static Set<String> getSiblingsSymbols(Set<Node> siblings) {
        return siblings.stream().map(s -> s.symbol).collect(Collectors.toSet());
    }


}