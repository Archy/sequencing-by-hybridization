public class IllegalGraph extends RuntimeException {
    public IllegalGraph(String message) {
        super(message);
    }
}
