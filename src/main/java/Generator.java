import com.google.common.annotations.VisibleForTesting;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Generator {
    public static void main(String[] args) throws IOException {
        String word = "ACACGCAACTTAAA";
        int k = 3;

        generateWords(word, k);
    }

    /**
     * Cuts the word to every possible substring of length k, shuffles them and saves in resources/words.txt
     */
    private static void generateWords(String word, int k) throws IOException {
        final Path filePath = Paths.get("src", "main", "resources", "words.txt").toAbsolutePath();
        List<String> substrings = cutWord(word, k);
        Collections.shuffle(substrings);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath.toFile(), false))) {
            for (String str: substrings) {
                writer.write(str);
                writer.newLine();
            }
        }
    }

    @VisibleForTesting
    static List<String> cutWord(String word, int k) {
        if (word.length() < k) {
            throw new IllegalArgumentException("Word must be longer than k");
        }
        final List<String> substrings = new ArrayList<>();

        for (int i=0; i<word.length() - k + 1; i++) {
            substrings.add(
                    word.substring(i, i+k)
            );
        }
        return substrings;
    }

}
