import com.google.common.annotations.VisibleForTesting;

import java.util.*;
import java.util.function.BiConsumer;

public class SequenceHybridizer {
    /**
     * Attempts to reconstruct word given it's substrings. Returns reconstructed word or nothing if reconstruction failed.
     */
    public Optional<String> reconstructWord(final List<String> subwords, final int k) {
        final Map<String, Node> nodes = createGraph(subwords, k);
        try {
            Optional<Node> startNode = findStartNode(nodes);

            if(!startNode.isPresent()){  // euler path
                startNode = nodes.values().stream().findFirst();
            }

            Node currentNode = startNode.get();
            StringBuilder result = new StringBuilder(currentNode.symbol);

            Node[] siblingsArray = currentNode.siblings.toArray(new Node[currentNode.siblings.size()]);
            Node sibling;
            for(int i = 0; i < siblingsArray.length; i++){
                sibling = siblingsArray[i];
                if(!isBridge(currentNode, sibling) ||
                        i == siblingsArray.length - 1){    // if edge is not bridge or it is the last edge

                    currentNode.removeSibling(sibling.symbol);           // remove the edge

                    currentNode = sibling;                          // move current node
                    siblingsArray = currentNode.siblings.toArray(new Node[currentNode.siblings.size()]);
                    i = -1;

                    result.append(currentNode.symbol.charAt(currentNode.symbol.length()-1));  // add the last character to superstring - result sequence
                }
            }

            return Optional.of(result.toString());

        } catch (IllegalGraph ignored) {
            return Optional.empty();
        }
    }

    /**
     * Finds and returns starting node if graph has only Eulerian path. Returns nothing if graph has Eulerian cycle.
     * Throws exception if graph is not an Eulerian graph.
     */
    @VisibleForTesting
    Optional<Node> findStartNode(final Map<String, Node> nodes) {
        final Map<String, Integer> nodesIndegrees = new HashMap<>();
        if (nodes.size() == 1) {
            return nodes.values().stream().findAny();
        }

        for (Node n: nodes.values()) {
            nodesIndegrees.putIfAbsent(n.symbol, 0);
            for(Node c: n.siblings) {
                nodesIndegrees.putIfAbsent(c.symbol, 0);
                nodesIndegrees.compute(c.symbol, (key, value) -> value+1);
            }
        }

        Optional<Node> startNode = Optional.empty();
        Optional<Node> endNode = Optional.empty();

        for (Node n: nodes.values()) {
            int inDegree = nodesIndegrees.get(n.symbol);
            int outDegree = n.siblings.size();

            if (inDegree == 0 && outDegree == 0) {
                throw new IllegalGraph("Disconnected node");
            }

            if (inDegree != outDegree) {
                if (outDegree > inDegree) {
                    // possible starting point
                    if (outDegree -inDegree != 1) {
                        throw new IllegalGraph("Difference between outDegree and inDegree is greater than 1");
                    }
                    if (!startNode.isPresent()) {
                        startNode = Optional.of(n);
                    } else {
                        throw new IllegalGraph("Multiple vertices with out degree > in degree");
                    }
                } else {
                    // possible ending point
                    if (inDegree - outDegree != 1) {
                        throw new IllegalGraph("Difference between inDegree and outDegree is greater than 1");
                    }
                    if (!endNode.isPresent()) {
                        endNode = Optional.of(n);
                    } else {
                        throw new IllegalGraph("Multiple vertices with in degree > out degree");
                    }
                }
            }
        }
        if ((startNode.isPresent() && !endNode.isPresent()) || (endNode.isPresent() && !startNode.isPresent())) {
            throw new IllegalGraph("Only one of (start node, end node) is present");
        }

        return startNode;
    }

    @VisibleForTesting
    Map<String, Node> createGraph(final List<String> subwords, final int k) {
        final Map<String, Node> nodes = new HashMap<>();

        // create nodes
        iterateSubwords(subwords, k, (s1, s2) -> {
            nodes.computeIfAbsent(s1, Node::new);
            nodes.computeIfAbsent(s2, Node::new);
        });

        // create edges
        iterateSubwords(subwords, k, (s1, s2) -> {
            Node node1 = nodes.get(s1);
            Node node2 = nodes.get(s2);
            node1.addSibling(node2);
        });

        return nodes;
    }

    boolean isBridge(final Node u, final Node v){
        if(u.symbol.equals(v.symbol))
            return false;

        int count1 = countReachableNodes(u);     // count nodes reachable from u
        u.removeSibling(v.symbol);                           // remove edge between u and v
        int count2 = countReachableNodes(u);     // count once again
        u.addSibling(v);                                // restore sibling

        return count1 > count2;                         // if number of reachable nodes changed - edge is the bridge
    }

    private int countReachableNodes(Node startNode) {
        Map<String, Boolean> visitedNodes = new HashMap<>();
        visitedNodes.putIfAbsent(startNode.symbol, true);
        for(Node sibling : startNode.siblings){
            DFS(sibling, visitedNodes);
        }
        return visitedNodes.size();
    }

    private void DFS(Node currentNode, Map<String, Boolean> visitedNodes) {
        visitedNodes.putIfAbsent(currentNode.symbol, true);
        for(Node sibling : currentNode.siblings){
            if(!visitedNodes.containsKey(sibling.symbol)) {
                DFS(sibling, visitedNodes);
            }
        }
    }

    private static void iterateSubwords(final List<String> subwords, final int k, final BiConsumer<String, String> consumer) {
        for (String s: subwords) {
            final String s1 = s.substring(0, k-1);
            final String s2 = s.substring(1, k);
            consumer.accept(s1, s2);
        }
    }
}
